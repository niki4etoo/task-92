import classNames from "classnames";

import formatCurrency from './utils';

export default class Notification {
  static get types() {
    return {
      PEPPERONI: "pepperoni",
      MARGHERITA: "margherita",
      HAWAIIAN: "hawaiian",
    };
  }

  constructor() {
    this.container = document.createElement("div");
    this.container.classList.add("notification-container");
  }

  render({type, price}) {
	this._type = type;
	this._price = formatCurrency(price);
    const template = `
<div class="notification type-${type} ${classNames({
      "is-danger": this._type === Notification.types.HAWAIIAN,
    })}">
  <button class="delete"></button>
  🍕 <span class="type">${type}</span> (<span class="price">${this._price}</span>) has been added to your order.
</div>
    `;
	console.log(this.container);
    return this.container.innerHTML = template;
  }
  
  empty(){
	  
  }
  
}
